package features;

import java.util.Random;

public class Funcs {
    public static String word(int a, String[] words) {
        int word = 0;
        int n = a;
        if (n > 99)
            n = toTwo(n);

        if (n >= 21 && n <= 99)
            n = toOne(n);

        if (n >= 10 && n <= 20 || n >= 5 || n == 0)
            word = 2;
        else if (n >= 2 && n <= 4)
            word = 1;

        return a + " " + words[word];
    }

    public static int toTwo(int a) {
        String s = Integer.toString(a);
        a = Integer.parseInt(s.substring(s.length() - 2));
        return a;
    }

    public static int toOne(int a) {
        String s = Integer.toString(a);
        a = Integer.parseInt(s.substring(s.length() - 1));
        return a;
    }

    public static double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();
        long factor = (long) Math.pow(10, places);
        value = value * factor;
        long tmp = Math.round(value);
        return (double) tmp / factor;
    }

    public static int rangeInt(int min, int max) {
        Random rnd = new Random();
        return min + rnd.nextInt(max - min + 1);
    }

    public static double rangeDouble(int min, int max) {
        Random rnd = new Random();
        return round(min + (max - min) * rnd.nextDouble(), 2);
    }
}
