package models;

import java.util.ArrayList;
import java.util.concurrent.Semaphore;

import static features.Funcs.*;

public class GoldMine {
    private int miners = 0;
    private int gold;
    private ArrayList<Shaft> shafts;
    private int minersPerShaft;
    Semaphore semaphore;

    static final int goldMin = 1000;
    static final int goldMax = 5000;

    static final int defaultMinersPerShaft = 3;

    static final int shaftMaxGoldCapacity = rangeInt(300, 700);

    public GoldMine() {
        this.gold = rangeInt(goldMin, goldMax);
        this.shafts = shaftsGenerate();
        this.minersPerShaft = defaultMinersPerShaft;
        this.semaphore = new Semaphore(shafts.size() * this.minersPerShaft);
    }

    public GoldMine(int gold) {
        if (gold < goldMin)
            this.gold = goldMin;
        else if (gold > goldMax)
            this.gold = goldMax;
        else
            this.gold = gold;

        this.shafts = shaftsGenerate();
        this.minersPerShaft = defaultMinersPerShaft;
        this.semaphore = new Semaphore(shafts.size() * this.minersPerShaft);
    }

    public GoldMine(int gold, int minersPerShaft) {
        if (gold < goldMin)
            this.gold = goldMin;
        else if (gold > goldMax)
            this.gold = goldMax;
        else
            this.gold = gold;

        this.shafts = shaftsGenerate();
        this.minersPerShaft = minersPerShaft;
        this.semaphore = new Semaphore(shafts.size() * this.minersPerShaft);
    }

    ArrayList<Shaft> shaftsGenerate() {
        int goldTmp = this.gold;
        int count = goldTmp / shaftMaxGoldCapacity;

        ArrayList<Shaft> shafts = new ArrayList<>();

        for (int i = 0; i < count; i++)
            if (goldTmp - shaftMaxGoldCapacity > 0)
                shafts.add(new Shaft(shaftMaxGoldCapacity, this.minersPerShaft));
            else
                shafts.add(new Shaft(goldTmp, this.minersPerShaft));

        return shafts;
    }


    public ArrayList<Shaft> getShafts() {
        return shafts;
    }

    public int getMinersPerShaft() {
        return minersPerShaft;
    }

    public void closeEmptyShafts() {
        for (Shaft shaft : shafts) {
            if (shaft.isEmpty())
                shafts.remove(shaft);
        }
    }

    public void closeShaft(Shaft shaft){
        shafts.remove(shaft);
    }
}
