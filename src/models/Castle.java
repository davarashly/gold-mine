package models;

import java.util.concurrent.Semaphore;

public class Castle {
    private int capacity;
    private int gold;
    private Semaphore semaphore;

    public Castle() {
        this.capacity = 5;
        this.gold = 0;
        this.semaphore = new Semaphore(this.capacity);
    }

    public void putGold(int gold) {
        this.gold += gold;
    }
}
