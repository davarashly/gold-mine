package models;

import java.util.Random;
import java.util.concurrent.Semaphore;

public class Shaft {
    private int gold;
    private int capacity;
    private Semaphore semaphore;

    Shaft(int gold, int capacity) {
        this.gold = gold;
        this.capacity = capacity;
        this.semaphore = new Semaphore(capacity);
    }

    public synchronized int getGoldFromShaft(int x) {
        Random random = new Random();
        if (x < this.gold) {
            this.gold -= x;
            return x;
        } else {
            int tmp = this.gold;
            this.gold = 0;
            return tmp;
        }
    }

    public int getGold() {
        return gold;
    }

    public void setGold(int gold) {
        this.gold = gold;
    }

    public int getCapacity() {
        return capacity;
    }

    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }

    public Semaphore getSemaphore() {
        return semaphore;
    }

    public void setSemaphore(Semaphore semaphore) {
        this.semaphore = semaphore;
    }

    public void minerIn() {
        this.capacity++;
    }

    public void minerOut() {
        this.capacity--;
    }

    public boolean isEmpty() {
        if (this.gold > 0)
            return false;
        else
            return true;
    }
}