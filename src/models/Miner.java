package models;

import static features.Funcs.*;

public class Miner implements Runnable {
    private String name;
    private int pocket;
    private int goldPerTime;
    private int pocketCapacity;
    private int sleepTime; // in secs;

    private GoldMine goldMine;
    private Shaft shaft;

    public Miner(String name, GoldMine goldMine) {
        this.name = name;
        this.pocket = 0;
        this.goldPerTime = rangeInt(20, 40);
        this.pocketCapacity = rangeInt(120, 200);
        this.sleepTime = rangeInt(2, 6);
        this.goldMine = goldMine;
    }

    public void run() {
        shaftSearch();
        shaftEnter();
        mining();
        shaftQuit();


        // putting gold int the castle
    }

    void shaftEnter() {
        this.shaft.minerIn();
        try {
            this.shaft.getSemaphore().acquire();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    void shaftQuit() {
        this.shaft.minerOut();
        this.shaft.getSemaphore().release();
        this.shaft = null;
    }

    void closeShaft() {
        try {
            System.out.println(name + ": Shaft is empty, closing the shaft...\n");
            this.goldMine.closeShaft(this.shaft);
            this.shaft = null;
            Thread.sleep(650);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    void shaftSearch() {
        System.out.println(name + ": Searching for the shaft...\n");
        while (this.shaft == null)
            for (Shaft shaft : goldMine.getShafts())
                if (shaft.getCapacity() < goldMine.getMinersPerShaft() && !shaft.isEmpty()) {
                    this.shaft = shaft;
                    System.out.println(name + ": Shaft is found!\n");
                    break;
                }
    }

    public void setShaft(Shaft shaft) {
        this.shaft = shaft;
    }

    public void mining() {
        System.out.println(name + ": Ready to work...");
        while (pocketCapacity > pocket) {
            pocket += shaft.getGoldFromShaft(goldPerTime);
            System.out.println(name + ": In my pockets now :" + pocket);
            if (shaft.getGold() != 0)
                System.out.println(name + ": In shaft now — " + shaft.getGold() + "\n");
            else
                closeShaft();
            try {
                Thread.sleep(sleepTime * 1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
